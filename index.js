const fs = require("fs");
var each = require('foreach');


///// Const

const baseDiff = 1;
const divider = 200;

const kpsFingerSlope = 1.8;
const maxKpsDiff = 40;
const strainDecay = 2;

const sectionLength = 400;
const minLength = 60;

const weightSlope = 0.9;
const weightBase = 0.6;

const keyDiff = {
    "Left": 1,
    "Up": 1.15,
    "Down": 1.15,
    "Right": 1,
}


const maxSections = minLength / (sectionLength / 1000);

///// Declaration

function parseMap(path) {
    return JSON.parse(fs.readFileSync(path, { encoding: 'utf8' }).replace(/^\uFEFF/, ''));
}

function standardDeviation(values) {
    var avg = average(values);

    var squareDiffs = values.map(function (value) {
        var diff = value - avg;
        var sqrDiff = diff * diff;
        return sqrDiff;
    });

    var avgSquareDiff = average(squareDiffs);

    var stdDev = Math.sqrt(avgSquareDiff);
    return stdDev;
}

function average(data) {
    var sum = data.reduce(function (sum, value) {
        return sum + value;
    }, 0);

    var avg = sum / data.length;
    return avg;
}

function getDiff(sectionColumns, sectionLength, previousStrain) {
    var diff = 0;
    var diffs = [];

    each(sectionColumns, function (times, key) {
        if (times.length > 0) {
            var c = Math.min(((times.length ** kpsFingerSlope) * sectionLength / 1000) + standardDeviation(times), maxKpsDiff) * keyDiff[key];
            diff += c;
            diffs.push(c);
        }
    });

    var std = diffs.length > 0 ? standardDeviation(diffs) : 0;

    return diff + std + (previousStrain / strainDecay);
}

function getColumns(file) {
    var columns = {
        "Left": [],
        "Up": [],
        "Down": [],
        "Right": [],
    };

    var map = JSON.parse(fs.readFileSync(file));

    map.events.forEach(element => {

        if (element.data[0] == 'SpawnObj') {
            element.data[1].split("-").forEach(key => {
                key = key.replace(/[\[,\],\,,0-9]/g, "");
                columns[key].push(Math.floor(element.time * 1000));
            });
        }
    });

    return columns;
}

function getMapDifficulty(path) {
    var columns = getColumns(path);
    var diffs = [];

    var done = false;
    var t = 0;
    var current_strain = 0;

    while (!done) {
        var current_section = {
            "Left": [],
            "Up": [],
            "Down": [],
            "Right": [],
        };
        t += sectionLength;
        current_strain /= strainDecay;

        each(columns, function (times, key) {
            while (times[0] <= t) {
                current_section[key].push(times.shift());
            }
        });

        done = true;
        each(columns, function (times, key) {
            if (times.length != 0) {
                done = false;
            }
        });
        
        if (!done) {
            current_strain = getDiff(current_section, sectionLength, current_strain);
            diffs.push([current_strain, t]);
        }
    }

    function sortDiff(a, b) {
        return b[0] - a[0];
    }

    diffs.sort(sortDiff);

    var difficulty = 0;
    var weight = weightBase;
    for (i = 0; i < maxSections; i++) {
        if (diffs[i] == null) {
            i = maxSections;
        } else {
            difficulty += diffs[i][0] * weight;
            weight *= weightSlope;
        }
    }

    difficulty /= divider;
    return baseDiff + difficulty;
}

//////////////// Start

var path = "config.txt";
if (process.argv.length >= 3) {
    path = process.argv[2];
}
var list = false;
if (process.argv.length >= 4) {
    list = process.argv[3].toLowerCase() == "list";
}

let toProcess = [];
if (list) {
    fs.readdirSync(path).forEach(file => {
        var map = path + "/" + file;
        toProcess.push(map);
    });

} else {
    toProcess.push(path);
}

toProcess.forEach(map => {
    try {
        var parsed = parseMap(map);
        console.log(getMapDifficulty(map).toFixed(2) + "," + parsed.name.replace(/[\,,\n,\r]/g, ""));
    } catch (e) {
        try {
            //console.log(parsed.name + " | Incorrect map ("+e+")");
        } catch (e) {
            //console.log(map + " cannot be parsed as JSON ("+e+")");
        }
    }
});